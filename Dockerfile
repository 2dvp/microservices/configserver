FROM maven:3.6.3-jdk-11 AS build
LABEL maintainer="IAOT - FIAP - pascoal.gabriel@gmail.com"
WORKDIR /home/app/
COPY . ./
RUN unset MAVEN_CONFIG && \
    ./mvnw -f /home/app/pom.xml clean package

FROM alpine
RUN apk --no-cache add openjdk11
COPY --from=build  /home/app/target/*.jar /microservices.jar
CMD [ "-jar", "/microservices.jar" ]
ENTRYPOINT [ "java" ]
